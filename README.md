Code Examples for Data Acquisition Course (first semester 2019)
===============================================================

This repository includes several examples for ESP32 MicroPython used in the
01219335 course (Data Acquisition and Integration) on the first semester of
2019 at Kasetsart University.

