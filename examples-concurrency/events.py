import time
from machine import Timer,Pin,I2C
from ssd1306 import SSD1306_I2C
 
led = Pin(5,Pin.OUT)
sw = Pin(22,Pin.IN)
i2c = I2C(scl=Pin(18),sda=Pin(19))
oled = SSD1306_I2C(128,64,i2c)
 
def show_text(text):
    oled.fill(0)
    oled.text(text,0,0)
    oled.show()
 
count = 0
show_text("count = {}".format(count))
 
def switch_pressed(pin):
    global count
    count = count+1
    show_text("count = {}".format(count))

def blink1(timer):
    led.value(0)  # turn LED on
    timer.init(period=100,
               mode=Timer.ONE_SHOT,
               callback=blink2)

def blink2(timer):
    led.value(1)  # turn LED off
    timer.init(period=1900,
               mode=Timer.ONE_SHOT,
               callback=blink1)

 
timer = Timer(0)
blink1(timer)
sw.irq(trigger=Pin.IRQ_FALLING,
       handler=switch_pressed)
 
while True:
    time.sleep(1)
