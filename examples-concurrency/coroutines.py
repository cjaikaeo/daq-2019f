import uasyncio as asyncio
from machine import Pin,I2C
from ssd1306 import SSD1306_I2C
 
led = Pin(5,Pin.OUT)
sw = Pin(22,Pin.IN)
i2c = I2C(scl=Pin(18),sda=Pin(19))
oled = SSD1306_I2C(128,64,i2c)

def show_text(text):
    oled.fill(0)
    oled.text(text,0,0)
    oled.show()

async def blink_led():
    while True:
        led.value(0)  # LED on
        await asyncio.sleep_ms(100)
        led.value(1)  # LED off
        await asyncio.sleep_ms(1900)

async def count_switch():
    count = 0
    show_text("count = {}".format(count))
    while True:
        while sw.value() == 1:
            await asyncio.sleep_ms(1)
        count = count+1
        show_text("count = {}".format(count))
        while sw.value() == 0:
            await asyncio.sleep_ms(1)

# create and run coroutines
loop = asyncio.get_event_loop()
loop.create_task(blink_led())
loop.create_task(count_switch())
loop.run_forever()
