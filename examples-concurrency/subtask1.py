from machine import Pin
import time
 
led = Pin(5, Pin.OUT)
while True:
    led.value(0)  # turn LED on
    time.sleep_ms(100)
    led.value(1)  # turn LED off
    time.sleep_ms(1900)
