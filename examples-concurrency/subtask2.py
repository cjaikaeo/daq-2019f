import time
from ssd1306 import SSD1306_I2C
from machine import Pin,I2C
 
sw = Pin(22,Pin.IN)
i2c = I2C(scl=Pin(18),sda=Pin(19))
oled = SSD1306_I2C(128,64,i2c)
 
def show_text(text):
    oled.fill(0)
    oled.text(text,0,0)
    oled.show()
 
count = 0
show_text("count = {}".format(count))
 
while True:
    while sw.value() == 1:
        time.sleep_ms(10)  # wait until switch is pressed
    count = count+1
    show_text("count = {}".format(count))
    while sw.value() == 0:
        time.sleep_ms(10)  # wait until switch is released

