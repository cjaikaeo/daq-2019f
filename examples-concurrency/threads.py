import _thread
import time
from machine import Pin,I2C
from ssd1306 import SSD1306_I2C

led = Pin(5,Pin.OUT)
sw = Pin(22,Pin.IN)
i2c = I2C(scl=Pin(18),sda=Pin(19))
oled = SSD1306_I2C(128,64,i2c)

def show_text(text):
    oled.fill(0)
    oled.text(text,0,0)
    oled.show()
 
def blink_led():
    while True:
        led.value(0)
        time.sleep_ms(100)
        led.value(1)
        time.sleep_ms(1900)

def count_switch():
    count = 0
    show_text("count = {}".format(count))
    while True:
        while sw.value() == 1:
            time.sleep_ms(10) # wait until sw is pressed
        count = count+1
        show_text("count = {}".format(count))
        while sw.value() == 0:
            time.sleep_ms(10) # wait until sw is released

# create and run threads
_thread.start_new_thread(blink_led, [])
_thread.start_new_thread(count_switch, [])

while True:
    time.sleep(1)
